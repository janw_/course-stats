coin = function (n = 100, prob = 0.5) 
{#Generates n coin tosses each with probability
  #of tossing a head equal to prob
  #and counts the number of heads in each toss;
  #output is the vector of number of heads in each coin toss.
  toss = runif(n)
  toss[toss < prob] = 1
  toss[toss < 1] = 0
  toss
}